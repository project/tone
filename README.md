Contents
--------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


Introduction
------------

The Tone module provides editor supplied frontend variations (CSS) as content
entities.

Caution: Allowing site users to inject frontend code is inherently dangerous.
Every site where this module is in use needs to be audited carefully. It is
recommended to restrict create/edit access to tone entities to trusted roles

 * For a full description of the module, visit the
   [project page](https://www.drupal.org/project/tone)

 * To submit bug reports and feature suggestions, or track changes, visit the
   [issue tracker](https://www.drupal.org/project/issues/tone)


Requirements
------------

This module requires no modules outside of Drupal core.


Recommended modules
-------------------

 * [Inline Entity Form](https://www.drupal.org/project/inline_entity_form):
   When enabled, tone entities can be created, edited and deleted easily from
   within the referencing content entity.


Installation
------------

 * Install as you would normally install a contributed Drupal module. Visit the
   [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
   documentation page for further information.


Configuration
-------------

 * Configure the user permissions in *Administration » People » Permissions*:

   - Administer tone types

     Users with this permission will be able to create, edit, delete tone
     entity types.

     Note: this permission should only be granted to trusted roles.

   - Administer tones, Create tone, Edit tone, Delete tone

     Users with this permission will be able to create, edit, delete tone
     entities.

     Note: depending on the tone type configuration, this permission permits
     frontend code injection. In this case this permission should only be
     granted to trusted roles.

 * Create and edit Tone Types in *Administration » Structure » Tone types*

   - Configure plugins which control how tone entities of a certain type
     are rendered and attached to the page.

   - Create fields which are used as source for user controlled values to be
     injected into the rendered source code.

 * Attach tones to content types in *Administration » Structure » Content types*

   - Create entity references to tone types in order to allow editors to choose
     from a curated list of tones.

   - Alternatively use *Inline Entity Forms* on the tone entity reference field
     and allow editors to create, edit and delete tone entities from within the
     content edit form.

   - Manage displays and choose which tone reference gets attached for every
     configured view mode: Drag the *Tone From X* extra field from the
     *Disabled* section into the active area. Also ensure that a matching tone
     reference field is present in the active area and the field formatter is
     set to *Rendered entity*.


Maintainers
-----------

Current maintainers:

 * [znerol](https://www.drupal.org/u/znerol)

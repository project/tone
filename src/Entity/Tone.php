<?php

namespace Drupal\tone\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Render\Element;
use Drupal\tone\ToneInterface;
use Drupal\tone\ToneTypeInterface;

/**
 * Defines the tone entity class.
 *
 * @ContentEntityType(
 *   id = "tone",
 *   label = @Translation("Tone"),
 *   label_collection = @Translation("Tones"),
 *   label_singular = @Translation("tone"),
 *   label_plural = @Translation("tones"),
 *   label_count = @PluralTranslation(
 *     singular = "@count tones",
 *     plural = "@count tones",
 *   ),
 *   bundle_label = @Translation("Tone type"),
 *   handlers = {
 *     "view_builder" = "Drupal\tone\ToneViewBuilder",
 *     "list_builder" = "Drupal\tone\ToneListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\tone\ToneAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\tone\Form\ToneForm",
 *       "edit" = "Drupal\tone\Form\ToneForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\tone\Routing\ToneHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "tone",
 *   data_table = "tone_field_data",
 *   revision_table = "tone_revision",
 *   revision_data_table = "tone_field_revision",
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   admin_permission = "administer tones",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "langcode" = "langcode",
 *     "bundle" = "bundle",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "collection" = "/admin/content/tone",
 *     "add-form" = "/tone/add/{tone_type}",
 *     "add-page" = "/tone/add",
 *     "canonical" = "/tone/{tone}",
 *     "edit-form" = "/tone/{tone}",
 *     "delete-form" = "/tone/{tone}/delete",
 *   },
 *   bundle_entity_type = "tone_type",
 *   field_ui_base_route = "entity.tone_type.edit_form",
 * )
 */
class Tone extends RevisionableContentEntityBase implements ToneInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the tone was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the tone was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(): void {
    $tone_type = $this->bundle->entity;
    assert($tone_type instanceof ToneTypeInterface);

    if ($tone_type->hasAttachmentStore()) {
      $identity = $tone_type->getIdentity();

      $displayRepository = \Drupal::service(EntityDisplayRepositoryInterface::class);
      assert($displayRepository instanceof EntityDisplayRepositoryInterface);

      $viewModes = $displayRepository->getViewModes('tone') ?: ['default'];
      foreach ($viewModes as $viewMode) {
        $identifier = $identity->getIdentifier($this, $viewMode);
        $tone_type->getAttachmentStore()->delete($identifier);
      }
    }

    parent::delete();
  }

  /**
   * {@inheritdoc}
   */
  public function attach(array $build, string $view_mode): array {
    $tone_type = $this->bundle->entity;
    assert($tone_type instanceof ToneTypeInterface);

    $identity = $tone_type->getIdentity();
    foreach ($identity->getAttributes($this, $view_mode) as $key => $value) {
      $build['#attributes'][$key] = $value;
    }

    $selector = $identity->getSelector($this, $view_mode);
    $source = $tone_type->getRenderer()->render($selector, $this, $view_mode);

    $identifier = $identity->getIdentifier($this, $view_mode);
    return $tone_type->getAttachmentStrategy()->attach($build, $identifier, $source);
  }

  /**
   * {@inheritdoc}
   */
  public function render(array $build, string $view_mode): array {
    $tone_type = $this->bundle->entity;
    assert($tone_type instanceof ToneTypeInterface);

    if ($tone_type->hasAttachmentStore()) {
      $identity = $tone_type->getIdentity();
      $selector = $identity->getSelector($this, $view_mode);
      $source = $tone_type->getRenderer()->render($selector, $this, $view_mode);

      $identifier = $identity->getIdentifier($this, $view_mode);
      $tone_type->getAttachmentStore()->put($identifier, $source);
    }

    // Hide all children.
    foreach (Element::children($build, FALSE) as $key) {
      $build[$key]['#access'] = FALSE;
    }

    return $build;
  }

}

<?php

namespace Drupal\tone\Entity;

use Drupal\Component\Plugin\LazyPluginCollection;
use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\Core\Plugin\DefaultSingleLazyPluginCollection;
use Drupal\tone\AttachmentStoreInterface;
use Drupal\tone\AttachmentStrategyInterface;
use Drupal\tone\AttachmentStrategyPluginManager;
use Drupal\tone\IdentityInterface;
use Drupal\tone\IdentityPluginManager;
use Drupal\tone\RendererInterface;
use Drupal\tone\RendererPluginManager;
use Drupal\tone\ToneTypeInterface;

/**
 * Defines the Tone type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "tone_type",
 *   label = @Translation("Tone type"),
 *   label_collection = @Translation("Tone types"),
 *   label_singular = @Translation("tone type"),
 *   label_plural = @Translation("tones types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count tones type",
 *     plural = "@count tones types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\tone\Form\ToneTypeForm",
 *       "edit" = "Drupal\tone\Form\ToneTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\tone\ToneTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer tone types",
 *   bundle_of = "tone",
 *   config_prefix = "type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/tone_types/add",
 *     "edit-form" = "/admin/structure/tone_types/manage/{tone_type}",
 *     "delete-form" = "/admin/structure/tone_types/manage/{tone_type}/delete",
 *     "collection" = "/admin/structure/tone_types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "selector",
 *     "identity",
 *     "identity_configuration",
 *     "renderer",
 *     "renderer_configuration",
 *     "attachment_strategy",
 *     "attachment_strategy_configuration"
 *   }
 * )
 */
class ToneType extends ConfigEntityBundleBase implements ToneTypeInterface, EntityWithPluginCollectionInterface {

  /**
   * The machine name of this tone type.
   */
  protected string $id = '';

  /**
   * The human-readable name of the tone type.
   */
  protected string $label = '';

  /**
   * The CSS selector used to match target elements.
   */
  protected string $selector = '';

  /**
   * The identity plugin instance ID.
   */
  protected string $identity = 'tone_entity_uuid';

  /**
   * The identity configuration.
   *
   * @var array<string, mixed>
   */
  protected array $identity_configuration = [];

  /**
   * The renderer plugin instance ID.
   */
  protected string $renderer = 'tone_css_property';

  /**
   * The renderer configuration.
   *
   * @var array<string, mixed>
   */
  protected array $renderer_configuration = [];

  /**
   * The attachment strategy plugin instance ID.
   */
  protected string $attachment_strategy = 'tone_css_public_files';

  /**
   * The attachment strategy configuration.
   *
   * @var array<string, mixed>
   */
  protected array $attachment_strategy_configuration = [];

  /**
   * The plugin collection that holds the renderer plugin for this entity.
   */
  protected ?LazyPluginCollection $identityPluginCollection = NULL;

  /**
   * The plugin collection that holds the renderer plugin for this entity.
   */
  protected ?LazyPluginCollection $rendererPluginCollection = NULL;

  /**
   * The plugin collection that hols the attachment strategy for this entity.
   */
  protected ?LazyPluginCollection $attachmentStrategyPluginCollection = NULL;

  /**
   * {@inheritdoc}
   */
  public function getSelector(): string {
    return $this->selector ?: '&';
  }

  /**
   * {@inheritdoc}
   */
  public function getIdentity(): IdentityInterface {
    return $this->getIdentityPluginCollection()->get($this->identity);
  }

  /**
   * {@inheritdoc}
   */
  public function getIdentityId(): string {
    return $this->identity;
  }

  /**
   * {@inheritdoc}
   */
  public function getIdentityConfiguration(): array {
    return (array) $this->identity_configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getRenderer(): RendererInterface {
    return $this->getRendererPluginCollection()->get($this->renderer);
  }

  /**
   * {@inheritdoc}
   */
  public function getRendererId(): string {
    return $this->renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function getRendererConfiguration(): array {
    return (array) $this->renderer_configuration;
  }

  /**
   * Returns true if an attachment store is configured on this tone type.
   */
  public function hasAttachmentStore(): bool {
    return $this->getAttachmentStrategy() instanceof AttachmentStoreInterface;
  }

  /**
   * Returns the attachment store.
   */
  public function getAttachmentStore(): AttachmentStoreInterface {
    $store = $this->getAttachmentStrategy();
    assert($store instanceof AttachmentStoreInterface);
    return $store;
  }

  /**
   * Returns the attachment strategy.
   */
  public function getAttachmentStrategy(): AttachmentStrategyInterface {
    return $this->getAttachmentStrategyPluginCollection()->get($this->attachment_strategy);
  }

  /**
   * Returns the attachment strategy plugin ID.
   */
  public function getAttachmentStrategyId(): string {
    return $this->attachment_strategy;
  }

  /**
   * Returns the attachment strategy plugin configuration.
   */
  public function getAttachmentStrategyConfiguration(): array {
    return (array) $this->attachment_strategy_configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return [
      'identity_configuration' => $this->getIdentityPluginCollection(),
      'renderer_configuration' => $this->getRendererPluginCollection(),
      'attachment_strategy_configuration' => $this->getAttachmentStrategyPluginCollection(),
    ];
  }

  /**
   * Encapsulates the creation of the tone identity LazyPluginCollection.
   */
  protected function getIdentityPluginCollection(): LazyPluginCollection {
    if (!$this->identityPluginCollection) {
      assert($this->identity);
      $this->identityPluginCollection = new DefaultSingleLazyPluginCollection(
        \Drupal::service(IdentityPluginManager::class),
        $this->identity,
        (array) $this->get('identity_configuration')
      );
    }
    return $this->identityPluginCollection;
  }

  /**
   * Encapsulates the creation of the tone renderer LazyPluginCollection.
   */
  protected function getRendererPluginCollection(): LazyPluginCollection {
    if (!$this->rendererPluginCollection) {
      assert($this->renderer);
      $this->rendererPluginCollection = new DefaultSingleLazyPluginCollection(
        \Drupal::service(RendererPluginManager::class),
        $this->renderer,
        (array) $this->get('renderer_configuration')
      );
    }
    return $this->rendererPluginCollection;
  }

  /**
   * Encapsulates the creation of the attachment strategy LazyPluginCollection.
   */
  protected function getAttachmentStrategyPluginCollection(): LazyPluginCollection {
    if (!$this->attachmentStrategyPluginCollection) {
      assert($this->attachment_strategy);
      $this->attachmentStrategyPluginCollection = new DefaultSingleLazyPluginCollection(
        \Drupal::service(AttachmentStrategyPluginManager::class),
        $this->attachment_strategy,
        (array) $this->get('attachment_strategy_configuration')
      );
    }
    return $this->attachmentStrategyPluginCollection;
  }

}

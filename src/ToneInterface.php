<?php

namespace Drupal\tone;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a tone entity type.
 */
interface ToneInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Add attributes and attachments for a tone to the given build array.
   *
   * @param mixed[] $build
   *   A render array.
   * @param string $view_mode
   *   The view mode.
   *
   * @return mixed[]
   *   The render array with tone customizations attached.
   */
  public function attach(array $build, string $view_mode): array;

  /**
   * Render and store source code for a tone.
   *
   * @param mixed[] $build
   *   A render array.
   * @param string $view_mode
   *   The view mode.
   *
   * @return mixed[]
   *   The render array with tone customizations attached.
   */
  public function render(array $build, string $view_mode): array;

}

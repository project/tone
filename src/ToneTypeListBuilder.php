<?php

namespace Drupal\tone;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of tone type entities.
 *
 * @see \Drupal\tone\Entity\ToneType
 */
class ToneTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   *
   * @phpstan-return mixed[]
   */
  public function buildHeader(): array {
    $header['title'] = $this->t('Label');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-return mixed[]
   */
  public function buildRow(EntityInterface $entity): array {
    $row['title'] = [
      'data' => $entity->label(),
      'class' => ['menu-label'],
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-return mixed[]
   */
  public function render(): array {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No tone types available. <a href=":link">Add tone type</a>.',
      [':link' => Url::fromRoute('entity.tone_type.add_form')->toString()]
    );

    return $build;
  }

}

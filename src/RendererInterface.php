<?php

namespace Drupal\tone;

/**
 * Interface for tone_renderer plugins.
 */
interface RendererInterface {

  /**
   * Returns the translated plugin label.
   */
  public function label(): string;

  /**
   * Returns the translated plugin description.
   */
  public function getDescription(): string;

  /**
   * Returns the source code for a tone.
   *
   * @param string $selector
   *   The selector to use.
   * @param \Drupal\tone\ToneInterface $tone
   *   The tone entity to render.
   * @param string $view_mode
   *   The view mode.
   */
  public function render(string $selector, ToneInterface $tone, string $view_mode): string;

}

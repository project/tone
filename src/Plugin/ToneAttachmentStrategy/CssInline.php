<?php

namespace Drupal\tone\Plugin\ToneAttachmentStrategy;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\tone\AttachmentStrategyPluginBase;
use Drupal\tone\Attribute\ToneAttachmentStrategy;

/**
 * Plugin implementation of the tone_attachment_strategy.
 */
#[ToneAttachmentStrategy(
  id: 'tone_css_inline',
  label: new TranslatableMarkup('CSS Inline'),
  description: new TranslatableMarkup('Adds a style tag to the HTML head.'),
)]
class CssInline extends AttachmentStrategyPluginBase {

  /**
   * {@inheritdoc}
   */
  public function attach(array $build, string $identifier, string $source): array {
    $styleTag = [
      '#tag' => 'style',
      '#value' => $source,
    ];

    $build['#attached']['html_head'][] = [
      $styleTag,
      'tone_css_inline-' . $identifier,
    ];

    return $build;
  }

}

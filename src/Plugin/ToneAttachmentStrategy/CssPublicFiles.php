<?php

namespace Drupal\tone\Plugin\ToneAttachmentStrategy;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\tone\AttachmentStoreInterface;
use Drupal\tone\AttachmentStrategyPluginBase;
use Drupal\tone\Attribute\ToneAttachmentStrategy;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the tone_attachment_strategy.
 */
#[ToneAttachmentStrategy(
  id: 'tone_css_public_files',
  label: new TranslatableMarkup('CSS Public Files'),
  description: new TranslatableMarkup('Stores a stylesheet in the public files directory and adds a link tag to the HTML head.'),
)]
class CssPublicFiles extends AttachmentStrategyPluginBase implements AttachmentStoreInterface, ContainerFactoryPluginInterface, PluginFormInterface, ConfigurableInterface {

  use StringTranslationTrait;

  /**
   * Whether or not to load the CSS after other content has loaded.
   */
  protected bool $loadAsync = FALSE;

  /**
   * Constructs a new CSS public files attachment strategy.
   *
   * @param mixed[] $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   The file url generator service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected FileUrlGeneratorInterface $fileUrlGenerator,
    protected FileSystemInterface $fileSystem,
    protected ConfigFactoryInterface $configFactory,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $configuration
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $fileUrlGenerator = $container->get(FileUrlGeneratorInterface::class);
    assert($fileUrlGenerator instanceof FileUrlGeneratorInterface);

    $fileSystem = $container->get(FileSystemInterface::class);
    assert($fileSystem instanceof FileSystemInterface);

    $configFactory = $container->get(ConfigFactoryInterface::class);
    assert($configFactory instanceof ConfigFactoryInterface);

    return new static($configuration, $plugin_id, $plugin_definition, $fileUrlGenerator, $fileSystem, $configFactory);
  }

  /**
   * {@inheritdoc}
   */
  public function attach(array $build, string $identifier, string $source): array {
    // Cache busting generated from hashed source.
    $b64hash = base64_encode(hash('fnv1a64', $source, TRUE));
    $bust = str_replace(['+', '/', '='], ['-', '_', ''], $b64hash);

    $uri = $this->generateUri($identifier);
    $link = [
      'rel' => 'stylesheet',
      'media' => 'all',
      'href' => $this->fileUrlGenerator->generateString($uri) . '?' . $bust,
    ];

    if ($this->loadAsync) {
      $link['media'] = 'all and (scripting: none)';
      $link['data-tone-async'] = TRUE;
      $build['#attached']['library'][] = 'tone/load_async';
    }

    $build['#attached']['html_head_link'][][] = $link;

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function put(string $identifier, string $source): bool {
    $uri = $this->generateUri($identifier);
    $path = dirname($uri);

    // Create the asset file.
    $this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);
    try {
      if (!$this->fileSystem->saveData($source, $uri, FileExists::Replace)) {
        return FALSE;
      }
    }
    catch (FileException $e) {
      return FALSE;
    }
    // If CSS/JS gzip compression is enabled and the zlib extension is available
    // then create a gzipped version of this file. This file is served
    // conditionally to browsers that accept gzip using .htaccess rules.
    // It's possible that the rewrite rules in .htaccess aren't working on this
    // server, but there's no harm (other than the time spent generating the
    // file) in generating the file anyway. Sites on servers where rewrite rules
    // aren't working can set css.gzip to FALSE in order to skip
    // generating a file that won't be used.
    $gzipEnabled = $this->configFactory->get('system.performance')->get('css.gzip');
    if (extension_loaded('zlib') && $gzipEnabled) {
      try {
        $encodedSource = gzencode($source, 9, FORCE_GZIP);
        if ($encodedSource === FALSE) {
          return FALSE;
        }
        if (!$this->fileSystem->saveData($encodedSource, $uri . '.gz', FileExists::Replace)) {
          return FALSE;
        }
      }
      catch (FileException $e) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(string $identifier): bool {
    $uri = $this->generateUri($identifier);

    $result = TRUE;
    $result &= @$this->fileSystem->unlink($uri);
    @$this->fileSystem->unlink($uri . '.gz');

    return (bool) $result;
  }

  /**
   * Returns the asset uri.
   */
  protected function generateUri(string $identifier): string {
    return "public://tone_css/tone-{$identifier}.css";
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $form
   * @phpstan-return mixed[]
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['load_async'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Load Async'),
      '#default_value' => $this->loadAsync,
      '#description' => $this->t('Use JavaScript to delay loading of the tone CSS until after other content has loaded.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $form
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $form
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->setConfiguration([
      'load_async' => (bool) $form_state->getValue('load_async'),
    ]);
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-return array<string, mixed>
   */
  public function getConfiguration() {
    return [
      'load_async' => (bool) $this->loadAsync,
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param array<string, mixed> $configuration
   */
  public function setConfiguration(array $configuration): void {
    $configuration += $this->defaultConfiguration();
    assert(is_bool($configuration['load_async']));
    $this->loadAsync = $configuration['load_async'];
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-return array<string, mixed>
   */
  public function defaultConfiguration() {
    return [
      'load_async' => FALSE,
    ];
  }

}

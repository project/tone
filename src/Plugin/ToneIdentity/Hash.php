<?php

namespace Drupal\tone\Plugin\ToneIdentity;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\tone\Attribute\ToneIdentity;
use Drupal\tone\IdentityInterface;
use Drupal\tone\IdentityPluginBase;
use Drupal\tone\ToneInterface;
use Drupal\tone\ToneTypeInterface;

/**
 * Plugin implementation of the tone_identity.
 */
#[ToneIdentity(
  id: 'tone_hash',
  label: new TranslatableMarkup('Hash'),
  description: new TranslatableMarkup('Computes a hash from the tone entity UUID, the bundle id and the view mode to generate the CSS selector. Useful in production since this mode results in shorter selectors.'),
)]
class Hash extends IdentityPluginBase implements IdentityInterface, PluginFormInterface, ConfigurableInterface {

  use StringTranslationTrait;

  /**
   * Hash algorithm.
   */
  protected string $algorithm = 'fnv1a64';

  /**
   * {@inheritdoc}
   */
  public function getIdentifier(ToneInterface $entity, string $view_mode): string {
    $data = implode("--", [
      $entity->bundle(),
      $entity->uuid(),
      $view_mode,
    ]);

    $hashRaw = base64_encode(hash($this->algorithm, $data, TRUE));
    return str_replace(['+', '/', '='], ['-', '_', ''], $hashRaw);
  }

  /**
   * {@inheritdoc}
   */
  public function getAttributes(ToneInterface $entity, string $view_mode): array {
    $identifier = $this->getIdentifier($entity, $view_mode);
    return [
      "data-tone-hash" => $identifier,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSelector(ToneInterface $entity, string $view_mode): string {
    $identifier = $this->getIdentifier($entity, $view_mode);
    $parentSelector = '[data-tone-hash="' . $identifier . '"]';

    $toneType = $entity->get('bundle')->entity;
    assert($toneType instanceof ToneTypeInterface);

    // Replace parent selector into tone type selector template.
    return str_replace('&', $parentSelector, $toneType->getSelector());
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $form
   * @phpstan-return mixed[]
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $algos = hash_algos();
    $form['algorithm'] = [
      '#type' => 'select',
      '#title' => $this->t('Algorithm'),
      '#required' => TRUE,
      '#options' => array_combine($algos, $algos),
      '#default_value' => $this->algorithm,
      '#description' => $this->t('Hash algorithm to use when generating identifier. It is recommended to use a non-cryptographic function with a size of up to 64 bits (e.g., xxh64 or fnv1a64).'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $form
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $form
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->setConfiguration([
      'algorithm' => $form_state->getValue('algorithm'),
    ]);
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-return array<string, mixed>
   */
  public function getConfiguration(): array {
    return [
      'algorithm' => (string) $this->algorithm,
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param array<string, mixed> $configuration
   */
  public function setConfiguration(array $configuration): void {
    $configuration += $this->defaultConfiguration();
    assert(is_string($configuration['algorithm']));
    $this->algorithm = $configuration['algorithm'];
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-return array<string, mixed>
   */
  public function defaultConfiguration(): array {
    return [
      'algorithm' => 'fnv1a64',
    ];
  }

}

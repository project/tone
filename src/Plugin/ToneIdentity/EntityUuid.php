<?php

namespace Drupal\tone\Plugin\ToneIdentity;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\tone\Attribute\ToneIdentity;
use Drupal\tone\IdentityPluginBase;
use Drupal\tone\ToneInterface;
use Drupal\tone\ToneTypeInterface;

/**
 * Plugin implementation of the tone_identity.
 */
#[ToneIdentity(
  id: 'tone_entity_uuid',
  label: new TranslatableMarkup('Entity UUID'),
  description: new TranslatableMarkup('Uses the tone entity UUID, the bundle id and the view mode in clear text to generate the CSS selector. Useful during development since this mode makes it easy to correlate selectors to entities.'),
)]
class EntityUuid extends IdentityPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getIdentifier(ToneInterface $entity, string $view_mode): string {
    $viewModeSanitized = preg_replace('/[^a-z0-9]/i', '-', $view_mode);
    assert($viewModeSanitized !== NULL);
    return implode('--', [$entity->uuid(), strtolower($viewModeSanitized)]);
  }

  /**
   * {@inheritdoc}
   */
  public function getAttributes(ToneInterface $entity, string $view_mode): array {
    $identifier = $this->getIdentifier($entity, $view_mode);
    $name = $this->getName($entity);
    return [
      "data-{$name}" => $identifier,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSelector(ToneInterface $entity, string $view_mode): string {
    $identifier = $this->getIdentifier($entity, $view_mode);
    $name = $this->getName($entity);
    $parentSelector = '[data-' . $name . '="' . $identifier . '"]';

    $toneType = $entity->get('bundle')->entity;
    assert($toneType instanceof ToneTypeInterface);

    // Replace parent selector into tone type selector template.
    return str_replace('&', $parentSelector, $toneType->getSelector());
  }

  /**
   * Return data attribute prefix for given entity.
   */
  protected function getName(ToneInterface $entity): string {
    $bundleSanitized = preg_replace('/[^a-z0-9]/i', '-', $entity->bundle());
    assert($bundleSanitized !== NULL);
    return implode('--', ['tone', strtolower($bundleSanitized)]);
  }

}

<?php

namespace Drupal\tone\Plugin\ToneRenderer;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\tone\Attribute\ToneRenderer;
use Drupal\tone\RendererInterface;
use Drupal\tone\RendererPluginBase;

/**
 * Plugin implementation of the tone_renderer.
 */
#[ToneRenderer(
  id: 'tone_css_property',
  label: new TranslatableMarkup('Single CSS Property'),
  description: new TranslatableMarkup('Sets a specified CSS property to the value stored in a field and applies the resulting CSS declaration to all elements matched by a given selector.'),
)]
class CssProperty extends RendererPluginBase implements RendererInterface, PluginFormInterface, ConfigurableInterface {

  use StringTranslationTrait;

  /**
   * The CSS property.
   */
  protected string $propertyName = '';

  /**
   * The field name.
   */
  protected string $fieldName = '';

  /**
   * {@inheritdoc}
   */
  public function render(string $selector, ContentEntityInterface $entity, string $view_mode): string {
    $result = '';

    $properties = array_filter(explode(" ", $this->propertyName));
    if (
      !empty($properties) &&
      !empty($this->fieldName) &&
      $entity->hasField($this->fieldName) &&
      !$entity->{$this->fieldName}->isEmpty()
    ) {
      $value = $this->sanitize($entity->{$this->fieldName}->first()->value);
      $declarations = array_map(fn($name) => "{$name}: {$value}", $properties);
      $style = implode("; ", $declarations);
      $result = "{$selector} { {$style} }";
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $form
   * @phpstan-return mixed[]
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['property_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CSS Property'),
      '#required' => TRUE,
      '#default_value' => $this->propertyName,
      '#description' => $this->t('CSS property name, e.g. <code>color</code> or <code>background</code> or some <a href=":mdn_link">CSS custom property</a> supported by the current theme. Multiple property names can be separated with whitespace. In this case the same value is assigned to all specified properties.', [
        ':mdn_link' => 'https://developer.mozilla.org/docs/Web/CSS/Using_CSS_custom_properties',
      ]),
    ];

    $form['field_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value field'),
      '#required' => TRUE,
      '#default_value' => $this->fieldName,
      '#description' => $this->t('Machine name of the field providing the property value. The field must exist on the tone type. Use <em>label</em> in order to select the entity label field as the source of the property value.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $form
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $form
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->setConfiguration([
      'property_name' => $form_state->getValue('property_name'),
      'field_name' => $form_state->getValue('field_name'),
    ]);
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-return array<string, mixed>
   */
  public function getConfiguration() {
    return [
      'property_name' => (string) $this->propertyName,
      'field_name' => (string) $this->fieldName,
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param array<string, mixed> $configuration
   */
  public function setConfiguration(array $configuration): void {
    $configuration += $this->defaultConfiguration();
    assert(is_string($configuration['property_name']));
    $this->propertyName = $configuration['property_name'];
    assert(is_string($configuration['field_name']));
    $this->fieldName = $configuration['field_name'];
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-return array<string, mixed>
   */
  public function defaultConfiguration() {
    return [
      'property_name' => 'color',
      'field_name' => 'field_color',
    ];
  }

  /**
   * Sanitize a property value.
   *
   * @param string $value
   *   A user supplied CSS property value.
   */
  protected function sanitize(string $value): string {
    $value = trim($value);

    foreach (["{", "}", ";"] as $separator) {
      [$value] = explode($separator, $value);
    }

    return $value;
  }

}

<?php

namespace Drupal\tone\Plugin\ToneRenderer;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\tone\Attribute\ToneRenderer;
use Drupal\tone\RendererInterface;
use Drupal\tone\RendererPluginBase;

/**
 * Plugin implementation of the tone_renderer.
 */
#[ToneRenderer(
  id: 'tone_css_declaration_block',
  label: new TranslatableMarkup('CSS Declaration Block'),
  description: new TranslatableMarkup('Applies the CSS declaration block from field to all elements matched by a given selector.'),
)]
class CssDeclarationBlock extends RendererPluginBase implements RendererInterface, PluginFormInterface, ConfigurableInterface {

  use StringTranslationTrait;

  /**
   * The field name.
   */
  protected string $fieldName = '';

  /**
   * {@inheritdoc}
   */
  public function render(string $selector, ContentEntityInterface $entity, string $view_mode): string {
    $result = '';

    if (
      !empty($this->fieldName) &&
      $entity->hasField($this->fieldName) &&
      !$entity->{$this->fieldName}->isEmpty()
    ) {
      $style = $this->sanitize($entity->{$this->fieldName}->first()->value);
      $result = "{$selector} { {$style} }";
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $form
   * @phpstan-return mixed[]
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['field_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Style field'),
      '#required' => TRUE,
      '#default_value' => $this->fieldName,
      '#description' => $this->t('Machine name of style field. The field must exist on the tone type. Use <em>label</em> in order to select the entity label field as the source of the CSS declaration block.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $form
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $form
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->setConfiguration([
      'field_name' => $form_state->getValue('field_name'),
    ]);
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-return array<string, mixed>
   */
  public function getConfiguration(): array {
    return [
      'field_name' => (string) $this->fieldName,
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param array<string, mixed> $configuration
   */
  public function setConfiguration(array $configuration): void {
    $configuration += $this->defaultConfiguration();
    assert(is_string($configuration['field_name']));
    $this->fieldName = $configuration['field_name'];
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-return array<string, mixed>
   */
  public function defaultConfiguration() {
    return [
      'field_name' => 'field_style',
    ];
  }

  /**
   * Sanitize a property value.
   *
   * @param string $value
   *   A user supplied CSS property value.
   */
  protected function sanitize(string $value): string {
    $value = trim($value);

    foreach (["{", "}"] as $separator) {
      [$value] = explode($separator, $value);
    }

    return $value;
  }

}

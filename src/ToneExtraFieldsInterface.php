<?php

namespace Drupal\tone;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Interface for a service responsible to collect extra field information.
 *
 * @phpstan-type ExtraFieldRecord array{
 *   'label': \Drupal\Core\StringTranslation\TranslatableMarkup,
 *   'description': \Drupal\Core\StringTranslation\TranslatableMarkup,
 *   'weight': int,
 *   'visible': bool
 * }
 */
interface ToneExtraFieldsInterface {

  /**
   * Returns extra field info as expected by hook_entity_extra_field_info().
   *
   * @return array<string, array<string, array<'form'|'display', array<string, ExtraFieldRecord>>>>
   *   Extra field info.
   */
  public function getExtraFieldInfo(): array;

  /**
   * Attach tone extra fields for the given entity, view_mode, display.
   *
   * @param mixed[] $build
   *   A render array.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity which is rendered.
   * @param string $view_mode
   *   The view mode an entity is rendered in.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface|null $display
   *   The display settings if the view mode is set to 'custom'.
   *
   * @return mixed[]
   *   The render array with tone customizations attached.
   */
  public function attach(array $build, EntityInterface $entity, string $view_mode, ?EntityViewDisplayInterface $display = NULL): array;

}

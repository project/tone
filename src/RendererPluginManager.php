<?php

namespace Drupal\tone;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\tone\Attribute\ToneRenderer;

/**
 * ToneRenderer plugin manager.
 */
class RendererPluginManager extends DefaultPluginManager {

  /**
   * Constructs RendererPluginManager object.
   *
   * @param \Traversable<string, string> $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/ToneRenderer',
      $namespaces,
      $module_handler,
      RendererInterface::class,
      ToneRenderer::class,
      'Drupal\tone\Annotation\ToneRenderer',
    );
    $this->alterInfo('tone_renderer_info');
    $this->setCacheBackend($cache_backend, 'tone_renderer_plugins');
  }

}

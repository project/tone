<?php

namespace Drupal\tone;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for tone_identity plugins.
 */
abstract class IdentityPluginBase extends PluginBase implements IdentityInterface {

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    // Cast the label to a string since it is a TranslatableMarkup object.
    assert(is_array($this->pluginDefinition));
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    // Cast the description to a string since it is a TranslatableMarkup object.
    assert(is_array($this->pluginDefinition));
    return (string) $this->pluginDefinition['description'];
  }

}

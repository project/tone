<?php

namespace Drupal\tone;

/**
 * Provides an interface defining the tone identity service.
 */
interface IdentityInterface {

  /**
   * Returns the translated plugin label.
   */
  public function label(): string;

  /**
   * Returns the translated plugin description.
   */
  public function getDescription(): string;

  /**
   * Returns a safe identifier for the given tone.
   *
   * The generated identifier is guaranteed to be filesystem safe.
   *
   * @param \Drupal\tone\ToneInterface $entity
   *   A tone entity.
   * @param string $view_mode
   *   A view mode.
   */
  public function getIdentifier(ToneInterface $entity, string $view_mode): string;

  /**
   * Returns the full CSS selector for a given tone.
   *
   * @param \Drupal\tone\ToneInterface $entity
   *   A tone entity.
   * @param string $view_mode
   *   A view mode.
   */
  public function getSelector(ToneInterface $entity, string $view_mode): string;

  /**
   * Return HTML attributes necessary to apply the tone to any element.
   *
   * @param \Drupal\tone\ToneInterface $entity
   *   A tone entity.
   * @param string $view_mode
   *   A view mode.
   *
   * @return array<string, string>
   *   Key-value pairs representing the HTML attributes.
   */
  public function getAttributes(ToneInterface $entity, string $view_mode): array;

}

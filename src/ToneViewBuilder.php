<?php

namespace Drupal\tone;

use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Render\Element;

/**
 * Provides a Tone view builder.
 */
class ToneViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $build_list
   * @phpstan-return mixed[]
   */
  public function buildMultiple(array $build_list): array {
    $build_list = parent::buildMultiple($build_list);
    return $this->eachBuild($build_list, [$this, 'renderTone']);
  }

  /**
   * Render a source code attachment represented by the given tone entity.
   *
   * @phpstan-param mixed[] $build
   * @phpstan-return mixed[]
   */
  protected function renderTone(array $build, ToneInterface $entity, string $view_mode): array {
    return $entity->render($build, $view_mode);
  }

  /**
   * Traverse build list and invoke callable for each build.
   *
   * @param mixed[] $build_list
   *   List of tone entity render arrays.
   * @param callable $callable
   *   The callback to invoke for each.
   *
   * @return mixed[]
   *   List of tone entity render arrays after the callback was invoked.
   *
   * @see \Drupal\Core\Entity\EntityViewBuilder::buildMultiple()
   */
  protected function eachBuild(array $build_list, callable $callable): array {
    // Build the view modes and display objects.
    $view_modes = [];
    $entity_type_key = "#{$this->entityTypeId}";

    // Find the keys for the ContentEntities in the build; Store entities for
    // rendering by view_mode.
    $children = Element::children($build_list);
    foreach ($children as $key) {
      if (isset($build_list[$key][$entity_type_key])) {
        $entity = $build_list[$key][$entity_type_key];
        if ($entity instanceof FieldableEntityInterface) {
          $view_modes[$build_list[$key]['#view_mode']][$key] = $entity;
        }
      }
    }

    // Build content for the displays represented by the entities.
    foreach ($view_modes as $view_mode => $view_mode_entities) {
      foreach (array_keys($view_mode_entities) as $key) {
        // Allow for alterations while building, before rendering.
        $entity = $build_list[$key][$entity_type_key];
        $build_list[$key] = $callable($build_list[$key], $entity, $view_mode);
      }
    }

    return $build_list;
  }

}

<?php

namespace Drupal\tone\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines tone_renderer annotation object.
 *
 * @Annotation
 */
class ToneRenderer extends Plugin {

  /**
   * The plugin ID.
   */
  public string $id;

  /**
   * The human-readable name of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public Translation $title;

  /**
   * The description of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public Translation $description;

}

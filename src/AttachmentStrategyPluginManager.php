<?php

namespace Drupal\tone;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\tone\Attribute\ToneAttachmentStrategy;

/**
 * ToneAttachmentStrategy plugin manager.
 */
class AttachmentStrategyPluginManager extends DefaultPluginManager {

  /**
   * Constructs AttachmentStrategyPluginManager object.
   *
   * @param \Traversable<string, string> $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/ToneAttachmentStrategy',
      $namespaces,
      $module_handler,
      AttachmentStrategyInterface::class,
      ToneAttachmentStrategy::class,
      'Drupal\tone\Annotation\ToneAttachmentStrategy'
    );
    $this->alterInfo('tone_attachment_strategy_info');
    $this->setCacheBackend($cache_backend, 'tone_attachment_strategy_plugins');
  }

}

<?php

namespace Drupal\tone;

/**
 * Interface for tone_attachment_store plugins.
 */
interface AttachmentStoreInterface {

  /**
   * Save an attachment source into the store.
   *
   * @param string $identifier
   *   The identifier used to retrieve the source later.
   * @param string $source
   *   The rendered attachment source.
   */
  public function put(string $identifier, string $source): bool;

  /**
   * Delete a stored attachment source.
   *
   * @param string $identifier
   *   The identifier for a stored version of the attachment source.
   */
  public function delete(string $identifier): bool;

}

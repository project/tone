<?php

namespace Drupal\tone\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines ToneIdentity attribute for plugin discovery.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class ToneIdentity extends Plugin {

  /**
   * Constructs a tone identity attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $label
   *   The human-readable name of the identity.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $description
   *   A brief description of the identity. This will be shown
   *   when adding or configuring this identity.
   */
  public function __construct(
    public readonly string $id,
    public readonly TranslatableMarkup $label,
    public readonly TranslatableMarkup $description,
  ) {
  }

}

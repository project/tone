<?php

namespace Drupal\tone\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines ToneAttachmentStrategy attribute for plugin discovery.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class ToneAttachmentStrategy extends Plugin {

  /**
   * Constructs a tone attachment strategy attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $label
   *   The human-readable name of the attachment strategy.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $description
   *   A brief description of the attachment strategy. This will be shown
   *   when adding or configuring this attachment strategy.
   */
  public function __construct(
    public readonly string $id,
    public readonly TranslatableMarkup $label,
    public readonly TranslatableMarkup $description,
  ) {
  }

}

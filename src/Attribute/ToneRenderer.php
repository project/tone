<?php

namespace Drupal\tone\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines ToneRenderer attribute for plugin discovery.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class ToneRenderer extends Plugin {

  /**
   * Constructs a tone renderer attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $label
   *   The human-readable name of the renderer.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $description
   *   A brief description of the renderer. This will be shown
   *   when adding or configuring this renderer.
   */
  public function __construct(
    public readonly string $id,
    public readonly TranslatableMarkup $label,
    public readonly TranslatableMarkup $description,
  ) {
  }

}

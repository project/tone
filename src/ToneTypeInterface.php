<?php

namespace Drupal\tone;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a tone type config entity.
 */
interface ToneTypeInterface extends ConfigEntityInterface {

  /**
   * Returns the CSS selector.
   *
   * @return string
   *   The CSS selector
   */
  public function getSelector(): string;

  /**
   * Returns the identity plugin instance.
   *
   * @return \Drupal\tone\IdentityInterface
   *   The identity plugin instance.
   */
  public function getIdentity(): IdentityInterface;

  /**
   * Returns the identity plugin ID.
   *
   * @return string
   *   The identity plugin id.
   */
  public function getIdentityId(): string;

  /**
   * Returns the identity plugin configuration.
   *
   * @return array<string, mixed>
   *   The identity plugin configuration.
   */
  public function getIdentityConfiguration(): array;

  /**
   * Returns the renderer plugin instance.
   *
   * @return \Drupal\tone\RendererInterface
   *   The renderer plugin instance.
   */
  public function getRenderer(): RendererInterface;

  /**
   * Returns the renderer plugin ID.
   *
   * @return string
   *   The renderer plugin ID.
   */
  public function getRendererId(): string;

  /**
   * Returns the renderer plugin configuration.
   *
   * @return array<string, mixed>
   *   The renderer plugin configuration.
   */
  public function getRendererConfiguration(): array;

  /**
   * Returns true if an attachment store is configured on this tone type.
   *
   * @return bool
   *   TRUE if an attachment store is configured.
   */
  public function hasAttachmentStore(): bool;

  /**
   * Returns the attachment store.
   *
   * @return \Drupal\tone\AttachmentStoreInterface
   *   The attachment store.
   */
  public function getAttachmentStore(): AttachmentStoreInterface;

  /**
   * Returns the attachment strategy.
   *
   * @return \Drupal\tone\AttachmentStrategyInterface
   *   The attachment strategy.
   */
  public function getAttachmentStrategy(): AttachmentStrategyInterface;

  /**
   * Returns the attachment strategy plugin ID.
   *
   * @return string
   *   The attachment strategy plugin ID.
   */
  public function getAttachmentStrategyId(): string;

  /**
   * Returns the attachment strategy plugin configuration.
   *
   * @return array<string, mixed>
   *   The attachment strategy plugin configuration.
   */
  public function getAttachmentStrategyConfiguration(): array;

}

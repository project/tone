<?php

namespace Drupal\tone;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

/**
 * Implementation of a service responsible to collect extra field information.
 *
 * @phpstan-import-type ExtraFieldRecord from ToneExtraFieldsInterface
 */
class ToneExtraFields implements ToneExtraFieldsInterface {

  use StringTranslationTrait;

  /**
   * Constructs a tone extra fields service.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   The entity type bundle info service.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entityDisplayRepository
   *   The entity display repository.
   * @param int $depthMax
   *   Maximum number of recursions a tone entity can be nested.
   */
  public function __construct(
    protected EntityFieldManagerInterface $entityFieldManager,
    protected EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    protected EntityDisplayRepositoryInterface $entityDisplayRepository,
    #[Autowire(param: 'tone.max_depth')]
    protected int $depthMax,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function getExtraFieldInfo(): array {
    $extra = [];

    $targets = [];
    foreach ($this->getAllBundlesForEntityType('tone') as $bundle) {
      $targets[] = 'tone:' . $bundle;
    }

    if (!empty($targets)) {
      $graph = $this->getReverseReferenceGraph();

      for ($depth = 0; $depth < $this->depthMax; $depth++) {
        $nextTargets = [];

        foreach ($targets as $target) {
          if (isset($graph[$target])) {
            foreach ($graph[$target] as $source => [$entityTypeId, $bundle, $sourceFieldName, $extraFieldName]) {
              $nextTargets[] = $source;
              $definition = $this->getExtraFieldDefinition(
                $entityTypeId,
                $bundle,
                $sourceFieldName,
                $depth
              );
              if ($definition !== FALSE) {
                $extra[$entityTypeId][$bundle]['display'][$extraFieldName] = $definition;
              }
            }
          }
        }

        $targets = $nextTargets;
      }
    }

    return $extra;
  }

  /**
   * {@inheritdoc}
   */
  public function attach(array $build, EntityInterface $entity, string $view_mode, ?EntityViewDisplayInterface $display = NULL): array {
    if (!$display) {
      $display = $this->entityDisplayRepository->getViewDisplay(
        $entity->getEntityTypeId(),
        $entity->bundle(),
        $view_mode
      );
    }

    $map = $this->getFieldMap();

    if (isset($map[$entity->getEntityTypeId()][$entity->bundle()])) {
      $fields = $map[$entity->getEntityTypeId()][$entity->bundle()];
      foreach ($fields as $extra_field_name => $source_field_name) {
        $source_settings = $display->getComponent($source_field_name);
        if ($source_settings && $display->getComponent($extra_field_name)) {
          $view_mode = $source_settings['settings']['view_mode'] ?? 'default';
          foreach ($entity->{$source_field_name} as $child) {
            if ($child->entity instanceof ToneInterface) {
              $build = $child->entity->attach($build, $view_mode);
            }
            else {
              $build = $this->attach($build, $child->entity, $view_mode);
            }
          }
        }
      }
    }

    return $build;
  }

  /**
   * Returns an array of extra field info keyed by entity id.
   *
   * Each entry is an associative array keyed by bundle where the value is
   * a map extra field name => source field name.
   *
   * @return array<string, array<string, array<string, string>>>
   *   Field map
   */
  protected function getFieldMap(): array {
    $result = [];

    foreach ($this->entityFieldManager->getFieldMapByFieldType('entity_reference') as $entity_type => $field_list) {
      assert(is_string($entity_type));
      assert(is_array($field_list));
      foreach ($field_list as $field_name => ['bundles' => $bundles]) {
        assert(is_string($field_name));
        foreach ($bundles as $bundle) {
          assert(is_string($bundle));
          $result[$entity_type][$bundle]['tone__' . $field_name] = $field_name;
        }
      }
    }

    return $result;
  }

  /**
   * Returns the reverse reference graph.
   *
   * The result is a two dimensional array with target_entity_id:target_bundle
   * as the keys on the first level, source_entity_id:source_bundle on the
   * second level. Values are quadruples of the form:
   *
   *   [source_entity_id, source_bundle, field_name, extra_field_name]
   *
   * @return array<string, array<string, array{string, string, string, string}>>
   *   The reverse reference graph.
   */
  protected function getReverseReferenceGraph(): array {
    $graph = [];

    foreach ($this->getFieldMap() as $entityTypeId => $bundles) {
      foreach ($bundles as $bundle => $fields) {
        $fieldDefinitions = $this->entityFieldManager->getFieldDefinitions($entityTypeId, $bundle);
        foreach ($fields as $extraFieldName => $sourceFieldName) {
          if (isset($fieldDefinitions[$sourceFieldName])) {
            $itemDefinition = $fieldDefinitions[$sourceFieldName]->getItemDefinition();

            $targetTypeId = $itemDefinition->getSetting('target_type');
            assert(is_string($targetTypeId));
            $handlerSettings = $itemDefinition->getSetting('handler_settings');
            assert(is_array($handlerSettings));
            $targetBundles = empty($handlerSettings['target_bundles']) ?
              $this->getAllBundlesForEntityType($targetTypeId) :
              $handlerSettings['target_bundles'];

            foreach ($targetBundles as $targetBundle) {
              $graph[$targetTypeId . ':' . $targetBundle][$entityTypeId . ':' . $bundle] = [
                $entityTypeId,
                $bundle,
                $sourceFieldName,
                $extraFieldName,
              ];
            }
          }
        }
      }
    }

    return $graph;
  }

  /**
   * Returns the extra field definition for the given source field.
   *
   * @param string $entityTypeId
   *   The entity type id to get information for.
   * @param string $bundle
   *   The bundle to get information for.
   * @param string $fieldName
   *   The field name to get information for.
   * @param int $depth
   *   The recursion depth.
   *
   * @return ExtraFieldRecord|false
   *   Extra field definition.
   */
  protected function getExtraFieldDefinition(string $entityTypeId, string $bundle, string $fieldName, int $depth): array|false {
    $result = FALSE;

    $definitions = $this->entityFieldManager->getFieldDefinitions($entityTypeId, $bundle);
    if (isset($definitions[$fieldName])) {
      $definition = $definitions[$fieldName];

      if ($depth === 0) {
        $result = [
          'label' => $this->t('Tone from @label', [
            '@label' => $definition->getLabel(),
          ]),
          'description' => $this->t('Apply a frontend variation from specified tone field to this entity'),
          'weight' => 0,
          'visible' => FALSE,
        ];
      }
      else {
        $result = [
          'label' => $this->t('Tones from @label (recursive)', [
            '@label' => $definition->getLabel(),
          ]),
          'description' => $this->t('Apply frontend variations from all tone fields referenced by the specified entity to this entity'),
          'weight' => 0,
          'visible' => FALSE,
        ];
      }
    }

    return $result;
  }

  /**
   * Gets all bundle IDs for a given entity type.
   *
   * @param string $entity_type_id
   *   The entity type for which to get bundles.
   *
   * @return string[]
   *   The bundle IDs.
   */
  protected function getAllBundlesForEntityType(string $entity_type_id): array {
    // Ensure all keys are strings because numeric values are allowed as bundle
    // names and "array_keys()" casts "42" to 42.
    return array_map('strval', array_keys($this->entityTypeBundleInfo->getBundleInfo($entity_type_id)));
  }

}

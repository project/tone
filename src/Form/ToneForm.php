<?php

namespace Drupal\tone\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the tone entity edit forms.
 */
class ToneForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $form
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New tone %label has been created.', $message_arguments));
        $this->logger('tone')->notice('Created new tone %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The tone %label has been updated.', $message_arguments));
        $this->logger('tone')->notice('Updated tone %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.tone.collection');

    return $result;
  }

}

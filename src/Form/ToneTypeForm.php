<?php

namespace Drupal\tone\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\tone\AttachmentStrategyPluginManager;
use Drupal\tone\IdentityPluginManager;
use Drupal\tone\RendererPluginManager;
use Drupal\tone\ToneTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for tone type forms.
 */
class ToneTypeForm extends BundleEntityFormBase {

  /**
   * Constructs a new tone type form.
   */
  public function __construct(
    protected IdentityPluginManager $identityPluginManager,
    protected RendererPluginManager $rendererPluginManager,
    protected AttachmentStrategyPluginManager $attachmentStrategyPluginManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get(IdentityPluginManager::class),
      $container->get(RendererPluginManager::class),
      $container->get(AttachmentStrategyPluginManager::class),
    );
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $form
   * @phpstan-return mixed[]
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $entity_type = $this->entity;
    assert($entity_type instanceof ToneTypeInterface);
    if ($this->operation == 'edit') {
      $form['#title'] = $this->t('Edit %label tone type', ['%label' => $entity_type->label()]);
    }

    $form['label'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $entity_type->label(),
      '#description' => $this->t('The human-readable name of this tone type.'),
      '#size' => 30,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#required' => TRUE,
      '#default_value' => $entity_type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => ['Drupal\tone\Entity\ToneType', 'load'],
        'source' => ['label'],
      ],
      '#description' => $this->t('A unique machine-readable name for this tone type. It must only contain lowercase letters, numbers, and underscores.'),
    ];

    $form['selector'] = [
      '#title' => $this->t('Selector'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $entity_type->getSelector(),
      '#description' => $this->t(
        'A CSS selector used to match elements on which the tone should be applied. The ampersand character (&) can be used in the same way as the <a href="https://sass-lang.com/documentation/style-rules/parent-selector">sass parent selector</a> to match the current entity.'
      ),
    ];

    $form['identity_plugin'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Identity Plugin'),
    ];

    $form['identity_plugin']['identity'] = [
      '#title' => $this->t('Identity'),
      '#type' => 'select',
      '#required' => TRUE,
      '#default_value' => $entity_type->getIdentityId(),
      '#options' => $this->getIdentityOptions(),
      '#empty_option' => $this->t('- Select -'),
      '#description' => $this->t('The identity plugin to use in order to generate CSS selectors for tones and data attributes for target entities.'),
      '#ajax' => [
        'callback' => '::updateIdentityConfiguration',
        'disable-refocus' => TRUE,
        'event' => 'change',
        'wrapper' => 'edit-tone-type-identity-configuration',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    $form['identity_plugin']['identity_configuration'] = [
      '#type' => 'container',
      '#prefix' => '<div id="edit-tone-type-identity-configuration">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
      '#parents' => ['identity_configuration'],
    ];

    if ($entity_type->getIdentityId()) {
      $identity = $entity_type->getIdentity();
      $form['identity_plugin']['identity_configuration']['about'] = [
        '#type' => 'item',
        '#weight' => -1,
        '#title' => $this->t('About the %plugin identity plugin', [
          '%plugin' => $identity->label(),
        ]),
        '#description' => $identity->getDescription(),
      ];
      if ($identity instanceof PluginFormInterface) {
        $subformState = SubformState::createForSubform(
          $form['identity_plugin']['identity_configuration'],
          $form, $form_state
        );
        $form['identity_plugin']['identity_configuration'] =
          $identity->buildConfigurationForm(
            $form['identity_plugin']['identity_configuration'],
            $subformState
          );
      }
    }

    $form['renderer_plugin'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Renderer Plugin'),
    ];

    $form['renderer_plugin']['renderer'] = [
      '#title' => $this->t('Renderer'),
      '#type' => 'select',
      '#required' => TRUE,
      '#default_value' => $entity_type->getRendererId(),
      '#options' => $this->getRendererOptions(),
      '#empty_option' => $this->t('- Select -'),
      '#description' => $this->t('The renderer plugin to use when generating source code for tone entities of this type.'),
      '#ajax' => [
        'callback' => '::updateRendererConfiguration',
        'disable-refocus' => TRUE,
        'event' => 'change',
        'wrapper' => 'edit-tone-type-renderer-configuration',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    $form['renderer_plugin']['renderer_configuration'] = [
      '#type' => 'container',
      '#prefix' => '<div id="edit-tone-type-renderer-configuration">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
      '#parents' => ['renderer_configuration'],
    ];

    if ($entity_type->getRendererId()) {
      $renderer = $entity_type->getRenderer();
      $form['renderer_plugin']['renderer_configuration']['about'] = [
        '#type' => 'item',
        '#weight' => -1,
        '#title' => $this->t('About the %plugin renderer plugin', [
          '%plugin' => $renderer->label(),
        ]),
        '#description' => $renderer->getDescription(),
      ];
      if ($renderer instanceof PluginFormInterface) {
        $subformState = SubformState::createForSubform(
          $form['renderer_plugin']['renderer_configuration'],
          $form, $form_state
        );
        $form['renderer_plugin']['renderer_configuration'] =
          $renderer->buildConfigurationForm(
            $form['renderer_plugin']['renderer_configuration'],
            $subformState
          );
      }
    }

    $form['attachment_strategy_plugin'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Attachment Strategy Plugin'),
    ];

    $form['attachment_strategy_plugin']['attachment_strategy'] = [
      '#title' => $this->t('Attachment Strategy'),
      '#type' => 'select',
      '#required' => TRUE,
      '#default_value' => $entity_type->getAttachmentStrategyId(),
      '#options' => $this->getAttachmentStrategyOptions(),
      '#empty_option' => $this->t('- Select -'),
      '#description' => $this->t('The attachment strategy plugin to use when adding rendered code to a page.'),
      '#ajax' => [
        'callback' => '::updateAttachmentStrategyConfiguration',
        'disable-refocus' => TRUE,
        'event' => 'change',
        'wrapper' => 'edit-tone-type-attachment-strategy-configuration',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    $form['attachment_strategy_plugin']['attachment_strategy_configuration'] = [
      '#type' => 'container',
      '#prefix' => '<div id="edit-tone-type-attachment-strategy-configuration">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
      '#parents' => ['attachment_strategy_configuration'],
    ];

    if ($entity_type->getAttachmentStrategyId()) {
      $strategy = $entity_type->getAttachmentStrategy();
      $form['attachment_strategy_plugin']['attachment_strategy_configuration']['about'] = [
        '#type' => 'item',
        '#weight' => -1,
        '#title' => $this->t('About the %plugin attachment strategy plugin', [
          '%plugin' => $strategy->label(),
        ]),
        '#description' => $strategy->getDescription(),
      ];
      if ($strategy instanceof PluginFormInterface) {
        $subformState = SubformState::createForSubform(
          $form['attachment_strategy_plugin']['attachment_strategy_configuration'],
          $form, $form_state
        );
        $form['attachment_strategy_plugin']['attachment_strategy_configuration'] =
          $strategy->buildConfigurationForm(
            $form['attachment_strategy_plugin']['attachment_strategy_configuration'],
            $subformState
          );
      }
    }

    return $this->protectBundleIdElement($form);
  }

  /**
   * Ajax callback for identity configuration.
   *
   * @param mixed[] $form
   *   The tone type form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed[]
   *   The identity configuration subform.
   */
  public static function updateIdentityConfiguration(array $form, FormStateInterface $form_state): array {
    return $form['identity_plugin']['identity_configuration'];
  }

  /**
   * Ajax callback for renderer configuration.
   *
   * @param mixed[] $form
   *   The tone type form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed[]
   *   The renderer configuration subform.
   */
  public static function updateRendererConfiguration(array $form, FormStateInterface $form_state): array {
    return $form['renderer_plugin']['renderer_configuration'];
  }

  /**
   * Ajax callback for attachment strategy configuration.
   *
   * @param mixed[] $form
   *   The tone type form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed[]
   *   The attachment strategy configuration subform.
   */
  public static function updateAttachmentStrategyConfiguration(array $form, FormStateInterface $form_state): array {
    return $form['attachment_strategy_plugin']['attachment_strategy_configuration'];
  }

  /**
   * {@inheritdoc}
   *
   * @param mixed[] $form
   *   The tone type form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed[]
   *   The action buttons.
   */
  protected function actions(array $form, FormStateInterface $form_state): array {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save tone type');
    $actions['delete']['#value'] = $this->t('Delete tone type');
    return $actions;
  }

  /**
   * {@inheritdoc}
   *
   * @param mixed[] $form
   *   The tone type form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $entity_type = $this->entity;
    assert($entity_type instanceof ToneTypeInterface);

    if ($entity_type->getIdentityId()) {
      $identity = $entity_type->getIdentity();
      if ($identity instanceof PluginFormInterface) {
        $subformState = SubformState::createForSubform(
          $form['identity_plugin']['identity_configuration'],
          $form, $form_state
        );
        $identity->validateConfigurationForm(
          $form['identity_plugin']['identity_configuration'],
          $subformState
        );
      }
    }

    if ($entity_type->getRendererId()) {
      $renderer = $entity_type->getRenderer();
      if ($renderer instanceof PluginFormInterface) {
        $subformState = SubformState::createForSubform(
          $form['renderer_plugin']['renderer_configuration'],
          $form, $form_state
        );
        $renderer->validateConfigurationForm(
          $form['renderer_plugin']['renderer_configuration'],
          $subformState
        );
      }
    }

    if ($entity_type->getAttachmentStrategyId()) {
      $strategy = $entity_type->getAttachmentStrategy();
      if ($strategy instanceof PluginFormInterface) {
        $subformState = SubformState::createForSubform(
          $form['attachment_strategy_plugin']['attachment_strategy_configuration'],
          $form, $form_state
        );
        $strategy->validateConfigurationForm(
          $form['attachment_strategy_plugin']['attachment_strategy_configuration'],
          $subformState
        );
      }
    }
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param mixed[] $form
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $entity_type = $this->entity;
    assert($entity_type instanceof ToneTypeInterface);

    $entity_type->set('id', trim((string) $entity_type->id()));
    $entity_type->set('label', trim((string) $entity_type->label()));
    $entity_type->set('selector', trim((string) $entity_type->getSelector()));

    if ($entity_type->getIdentityId()) {
      $identity = $entity_type->getIdentity();
      if ($identity instanceof PluginFormInterface) {
        $subformState = SubformState::createForSubform(
          $form['identity_plugin']['identity_configuration'],
          $form, $form_state
        );
        $identity->submitConfigurationForm(
          $form['identity_plugin']['identity_configuration'],
          $subformState
        );
      }
    }

    if ($entity_type->getRendererId()) {
      $renderer = $entity_type->getRenderer();
      if ($renderer instanceof PluginFormInterface) {
        $subformState = SubformState::createForSubform(
          $form['renderer_plugin']['renderer_configuration'],
          $form, $form_state
        );
        $renderer->submitConfigurationForm(
          $form['renderer_plugin']['renderer_configuration'],
          $subformState
        );
      }
    }

    if ($entity_type->getAttachmentStrategyId()) {
      $strategy = $entity_type->getAttachmentStrategy();
      if ($strategy instanceof PluginFormInterface) {
        $subformState = SubformState::createForSubform(
          $form['attachment_strategy_plugin']['attachment_strategy_configuration'],
          $form, $form_state
        );
        $strategy->submitConfigurationForm(
          $form['attachment_strategy_plugin']['attachment_strategy_configuration'],
          $subformState
        );
      }
    }

    $status = $entity_type->save();

    $t_args = ['%name' => $entity_type->label()];
    if ($status == SAVED_UPDATED) {
      $message = $this->t('The tone type %name has been updated.', $t_args);
      $this->messenger()->addStatus($message);
    }
    elseif ($status == SAVED_NEW) {
      $message = $this->t('The tone type %name has been added.', $t_args);
      $this->messenger()->addStatus($message);
    }

    $form_state->setRedirectUrl($entity_type->toUrl('collection'));

    return $status;
  }

  /**
   * Returns list of available identity plugins.
   *
   * @return array<string, string>
   *   List of available identity plugins.
   */
  protected function getIdentityOptions(): array {
    return array_map(
      fn ($definition) => $definition['label'],
      $this->identityPluginManager->getDefinitions()
    );
  }

  /**
   * Returns list of available renderer plugins.
   *
   * @return array<string, string>
   *   List of available render plugins.
   */
  protected function getRendererOptions(): array {
    return array_map(
      fn ($definition) => $definition['label'],
      $this->rendererPluginManager->getDefinitions()
    );
  }

  /**
   * Returns list of available attachment strategy plugins.
   *
   * @return array<string, string>
   *   List of available attachment strategy plugins.
   */
  protected function getAttachmentStrategyOptions(): array {
    return array_map(
      fn ($definition) => $definition['label'],
      $this->attachmentStrategyPluginManager->getDefinitions()
    );
  }

}

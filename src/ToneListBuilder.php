<?php

namespace Drupal\tone;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the tone entity type.
 */
class ToneListBuilder extends EntityListBuilder {

  /**
   * Constructs a new ToneListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, protected DateFormatterInterface $dateFormatter) {
    parent::__construct($entity_type, $storage);
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $entityTypeManager = $container->get(EntityTypeManagerInterface::class);
    assert($entityTypeManager instanceof EntityTypeManagerInterface);

    $dateFormatter = $container->get(DateFormatterInterface::class);
    assert($dateFormatter instanceof DateFormatterInterface);

    return new static(
      $entity_type,
      $entityTypeManager->getStorage($entity_type->id()),
      $dateFormatter,
    );
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-return mixed[]
   */
  public function render(): array {
    $build['table'] = parent::render();

    $total = $this->getStorage()
      ->getQuery()
      ->accessCheck(FALSE)
      ->count()
      ->execute();

    $build['summary']['#markup'] = $this->t('Total tones: @total', ['@total' => $total]);
    return $build;
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-return mixed[]
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('ID');
    $header['label'] = $this->t('Label');
    $header['created'] = $this->t('Created');
    $header['changed'] = $this->t('Updated');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-return mixed[]
   */
  public function buildRow(EntityInterface $entity): array {
    assert($entity instanceof ToneInterface);
    $row['id'] = $entity->id();
    $row['label'] = $entity->label();
    $createdTime = $entity->get('created')->value;
    assert(is_int($createdTime));
    $row['created'] = $this->dateFormatter->format($createdTime);
    $row['changed'] = $this->dateFormatter->format($entity->getChangedTime());
    return $row + parent::buildRow($entity);
  }

}

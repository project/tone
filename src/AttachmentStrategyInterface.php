<?php

namespace Drupal\tone;

/**
 * Interface for tone_attachment_strategy plugins.
 */
interface AttachmentStrategyInterface {

  /**
   * Returns the translated plugin label.
   */
  public function label(): string;

  /**
   * Returns the translated plugin description.
   */
  public function getDescription(): string;

  /**
   * Add the render array attachments for the source to the given build array.
   *
   * @param mixed[] $build
   *   Render array of some entity.
   * @param string $identifier
   *   The identifier used to retrieve a stored version of the source.
   * @param string $source
   *   The rendered attachment source.
   *
   * @return mixed[]
   *   Render array with tone customizations attached.
   */
  public function attach(array $build, string $identifier, string $source): array;

}

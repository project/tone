/**
 * @file
 * Delays loading of CSS until after other content has loaded.
 */

(function (Drupal, once) {
  /**
   * Search for async tone libraries and change the media.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches toneLoadAsync behavior.
   */
  Drupal.behaviors.toneLoadAsync = {
    attach(context) {
      once('tone-load-async', 'head [data-tone-async]', context).forEach(
        (l) => {
          l.setAttribute('media', 'all');
          l.removeAttribute('data-tone-async');
        },
      );
    },
  };
})(Drupal, once);

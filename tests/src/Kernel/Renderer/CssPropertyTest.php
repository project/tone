<?php

namespace Drupal\Tests\tone\Kernel\Renderer;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\tone\Traits\ToneTestTrait;
use Drupal\tone\Entity\Tone;
use Drupal\tone\Plugin\ToneRenderer\CssProperty;

/**
 * @coversDefaultClass \Drupal\tone\Plugin\ToneRenderer\CssProperty
 *
 * @group tone
 */
class CssPropertyTest extends KernelTestBase {

  use ToneTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['tone', 'user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('tone');
  }

  /**
   * Tests css property renderer with single property name.
   *
   * @covers ::render
   */
  public function testCssPropertyRendererSingle(): void {
    $selector = $this->randomMachineName();
    $propertyName = $this->randomMachineName();
    $fieldName = 'label';

    $rendererConfig = [
      'property_name' => $propertyName,
      'field_name' => $fieldName,
    ];

    $bundle = $this->createToneType(
      selector: $selector,
      renderer: 'tone_css_property',
      rendererConfiguration: $rendererConfig
    );

    $fieldValue = $this->randomMachineName();
    $tone = Tone::create([
      'label' => $fieldValue,
      'bundle' => $bundle->id(),
    ]);
    $tone->save();

    $renderer = $bundle->getRenderer();
    $this->assertInstanceOf(CssProperty::class, $renderer);

    $expected = "{$selector} { {$propertyName}: {$fieldValue} }";
    $result = $renderer->render($selector, $tone, 'default');
    $this->assertEquals($expected, $result);
  }

  /**
   * Tests css property renderer with multiple property names.
   *
   * @covers ::render
   */
  public function testCssPropertyRendererMulti(): void {
    $selector = $this->randomMachineName();
    $propertyName1 = $this->randomMachineName();
    $propertyName2 = $this->randomMachineName();
    $propertyName3 = $this->randomMachineName();
    $properties = $propertyName1 . ' ' . $propertyName2 . '  ' . $propertyName3;
    $fieldName = 'label';

    $rendererConfig = [
      'property_name' => $properties,
      'field_name' => $fieldName,
    ];

    $bundle = $this->createToneType(
      selector: $selector,
      renderer: 'tone_css_property',
      rendererConfiguration: $rendererConfig
    );

    $fieldValue = $this->randomMachineName();
    $tone = Tone::create([
      'label' => $fieldValue,
      'bundle' => $bundle->id(),
    ]);
    $tone->save();

    $renderer = $bundle->getRenderer();
    $this->assertInstanceOf(CssProperty::class, $renderer);

    $expected = "{$selector} { {$propertyName1}: {$fieldValue}; {$propertyName2}: {$fieldValue}; {$propertyName3}: {$fieldValue} }";
    $result = $renderer->render($selector, $tone, 'default');
    $this->assertEquals($expected, $result);
  }

}

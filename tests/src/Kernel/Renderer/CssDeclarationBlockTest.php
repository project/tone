<?php

namespace Drupal\Tests\tone\Kernel\Renderer;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\tone\Traits\ToneTestTrait;
use Drupal\tone\Entity\Tone;
use Drupal\tone\Plugin\ToneRenderer\CssDeclarationBlock;

/**
 * @coversDefaultClass \Drupal\tone\Plugin\ToneRenderer\CssDeclarationBlock
 *
 * @group tone
 */
class CssDeclarationBlockTest extends KernelTestBase {

  use ToneTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['tone', 'user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('tone');
  }

  /**
   * Tests css declaration block renderer.
   *
   * @covers ::render
   */
  public function testCssBlockRenderer(): void {
    $selector = $this->randomMachineName();
    $fieldName = 'label';

    $rendererConfig = [
      'field_name' => $fieldName,
    ];

    $bundle = $this->createToneType(
      selector: $selector,
      renderer: 'tone_css_declaration_block',
      rendererConfiguration: $rendererConfig
    );

    $fieldValue = "color: {$this->randomMachineName()}; background-color: {$this->randomMachineName()};";
    $tone = Tone::create([
      'label' => $fieldValue,
      'bundle' => $bundle->id(),
    ]);
    $tone->save();

    $renderer = $bundle->getRenderer();
    $this->assertInstanceOf(CssDeclarationBlock::class, $renderer);

    $expected = "{$selector} { {$fieldValue} }";
    $result = $renderer->render($selector, $tone, 'default');
    $this->assertEquals($expected, $result);
  }

}

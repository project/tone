<?php

namespace Drupal\Tests\tone\Kernel\AttachmentStrategy;

use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\tone\Traits\ToneTestTrait;
use Drupal\tone\Plugin\ToneAttachmentStrategy\CssPublicFiles;

/**
 * @coversDefaultClass \Drupal\tone\Plugin\ToneAttachmentStrategy\CssPublicFiles
 *
 * @group tone
 */
class CssPublicFilesTest extends KernelTestBase {

  use ToneTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system', 'tone', 'user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('tone');
  }

  /**
   * Tests CSS public files attachment strategy.
   *
   * @covers ::attach
   * @covers ::generateUri
   */
  public function testCssPublicFilesAttachmentStrategy(): void {
    $bundle = $this->createToneType(
      attachmentStrategy: 'tone_css_public_files'
    );

    $identifier = $this->randomMachineName();

    $build = [
      '#markup' => $this->randomString(),
    ];
    $expected = $build + [
      '#attached' => [
        'html_head_link' => [
          [
            [
              'rel' => 'stylesheet',
              'media' => 'all',
              // For 'href' key, see further down near the call to unset().
            ],
          ],
        ],
      ],
    ];

    $fileUrlGenerator = $this->container->get('file_url_generator');
    assert($fileUrlGenerator instanceof FileUrlGeneratorInterface);
    $expectedUrlPrefix = $fileUrlGenerator->generateString("public://tone_css/tone-{$identifier}.css");
    assert(!empty($expectedUrlPrefix));

    $attachmentStrategy = $bundle->getAttachmentStrategy();
    $this->assertInstanceOf(CssPublicFiles::class, $attachmentStrategy);

    $source1 = $this->randomMachineName();
    $source2 = $this->randomMachineName();

    $result1 = $attachmentStrategy->attach($build, $identifier, $source1);
    $url1 = $result1['#attached']['html_head_link'][0][0]['href'];
    $this->assertStringStartsWith($expectedUrlPrefix, $url1);
    unset($result1['#attached']['html_head_link'][0][0]['href']);
    $this->assertEquals($expected, $result1);

    $result2 = $attachmentStrategy->attach($build, $identifier, $source2);
    $url2 = $result2['#attached']['html_head_link'][0][0]['href'];
    $this->assertStringStartsWith($expectedUrlPrefix, $url2);
    unset($result2['#attached']['html_head_link'][0][0]['href']);
    $this->assertEquals($expected, $result2);

    $this->assertNotEquals($url2, $url1);
  }

  /**
   * Tests CSS public files async attachment strategy.
   *
   * @covers ::attach
   * @covers ::generateUri
   */
  public function testCssPublicFilesAsyncAttachmentStrategy(): void {
    $bundle = $this->createToneType(
      attachmentStrategy: 'tone_css_public_files',
      attachmentStrategyConfiguration: [
        'load_async' => TRUE,
      ],
    );

    $identifier = $this->randomMachineName();

    $build = [
      '#markup' => $this->randomString(),
    ];
    $expected = $build + [
      '#attached' => [
        'html_head_link' => [
          [
            [
              'rel' => 'stylesheet',
              'media' => 'all and (scripting: none)',
              'data-tone-async' => TRUE,
              // For 'href' key, see further down near the call to unset().
            ],
          ],
        ],
        'library' => [
          'tone/load_async',
        ],
      ],
    ];

    $fileUrlGenerator = $this->container->get('file_url_generator');
    assert($fileUrlGenerator instanceof FileUrlGeneratorInterface);
    $expectedUrlPrefix = $fileUrlGenerator->generateString("public://tone_css/tone-{$identifier}.css");
    assert(!empty($expectedUrlPrefix));

    $attachmentStrategy = $bundle->getAttachmentStrategy();
    $this->assertInstanceOf(CssPublicFiles::class, $attachmentStrategy);

    $source1 = $this->randomMachineName();
    $source2 = $this->randomMachineName();

    $result1 = $attachmentStrategy->attach($build, $identifier, $source1);
    $url1 = $result1['#attached']['html_head_link'][0][0]['href'];
    $this->assertStringStartsWith($expectedUrlPrefix, $url1);
    unset($result1['#attached']['html_head_link'][0][0]['href']);
    $this->assertEquals($expected, $result1);

    $result2 = $attachmentStrategy->attach($build, $identifier, $source2);
    $url2 = $result2['#attached']['html_head_link'][0][0]['href'];
    $this->assertStringStartsWith($expectedUrlPrefix, $url2);
    unset($result2['#attached']['html_head_link'][0][0]['href']);
    $this->assertEquals($expected, $result2);

    $this->assertNotEquals($url2, $url1);
  }

  /**
   * Tests CSS public files storage.
   *
   * @covers ::put
   * @covers ::delete
   * @covers ::generateUri
   */
  public function testCssPublicFilesAttachmentStore(): void {
    $bundle = $this->createToneType(
      attachmentStrategy: 'tone_css_public_files'
    );

    $attachmentStore = $bundle->getAttachmentStore();
    $this->assertInstanceOf(CssPublicFiles::class, $attachmentStore);

    $identifier = $this->randomMachineName();
    $source1 = $this->randomMachineName();
    $source2 = $this->randomMachineName();

    $uriPlain = "public://tone_css/tone-{$identifier}.css";
    $uriGzip = "public://tone_css/tone-{$identifier}.css.gz";

    // Perform tests with gzip disabled.
    $this->installConfig('system');
    $this->config('system.performance')->set('css.gzip', FALSE)->save();

    // Create.
    $this->assertFileDoesNotExist($uriPlain);
    $this->assertFileDoesNotExist($uriGzip);
    $result = $attachmentStore->put($identifier, $source1);
    $this->assertTrue($result);

    // Update.
    $this->assertFileExists($uriPlain);
    $this->assertFileDoesNotExist($uriGzip);
    $result = $attachmentStore->put($identifier, $source2);
    $this->assertTrue($result);

    // Delete.
    $this->assertFileExists($uriPlain);
    $this->assertFileDoesNotExist($uriGzip);
    $result = $attachmentStore->delete($identifier);
    $this->assertTrue($result);

    // Delete (non-existing)
    $this->assertFileDoesNotExist($uriPlain);
    $this->assertFileDoesNotExist($uriGzip);
    $result = $attachmentStore->delete($identifier);
    $this->assertFalse($result);

    // Perform tests with gzip enabled.
    $this->config('system.performance')->set('css.gzip', TRUE)->save();

    // Create.
    $this->assertFileDoesNotExist($uriPlain);
    $this->assertFileDoesNotExist($uriGzip);
    $result = $attachmentStore->put($identifier, $source1);
    $this->assertTrue($result);

    // Update.
    $this->assertFileExists($uriPlain);
    $this->assertFileExists($uriGzip);
    $result = $attachmentStore->put($identifier, $source2);
    $this->assertTrue($result);

    // Delete.
    $this->assertFileExists($uriPlain);
    $this->assertFileExists($uriGzip);
    $result = $attachmentStore->delete($identifier);
    $this->assertTrue($result);

    // Delete (non-existing)
    $this->assertFileDoesNotExist($uriPlain);
    $this->assertFileDoesNotExist($uriGzip);
    $result = $attachmentStore->delete($identifier);
    $this->assertFalse($result);
  }

}

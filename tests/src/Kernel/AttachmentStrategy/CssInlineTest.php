<?php

namespace Drupal\Tests\tone\Kernel\AttachmentStrategy;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\tone\Traits\ToneTestTrait;
use Drupal\tone\Plugin\ToneAttachmentStrategy\CssInline;

/**
 * @coversDefaultClass \Drupal\tone\Plugin\ToneAttachmentStrategy\CssInline
 *
 * @group tone
 */
class CssInlineTest extends KernelTestBase {

  use ToneTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['tone', 'user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('tone');
  }

  /**
   * Tests CSS inline attachment strategy.
   *
   * @covers ::attach
   */
  public function testCssInlineAttachmentStrategy(): void {
    $bundle = $this->createToneType(
      attachmentStrategy: 'tone_css_inline'
    );

    $source = $this->randomMachineName();
    $identifier = $this->randomMachineName();

    $build = [
      '#markup' => $this->randomString(),
    ];
    $expected = $build + [
      '#attached' => [
        'html_head' => [
          [
            ['#tag' => 'style', '#value' => $source],
            'tone_css_inline-' . $identifier,
          ],
        ],
      ],
    ];

    $attachmentStrategy = $bundle->getAttachmentStrategy();
    $this->assertInstanceOf(CssInline::class, $attachmentStrategy);

    $result = $attachmentStrategy->attach($build, $identifier, $source);
    $this->assertEquals($expected, $result);
  }

}

<?php

namespace Drupal\Tests\tone\Kernel;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\field\Traits\EntityReferenceFieldCreationTrait;
use Drupal\Tests\tone\Traits\ToneTestTrait;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\entity_test\Entity\EntityTestRev;
use Drupal\tone\Entity\Tone;
use Drupal\tone\ToneExtraFields;
use Drupal\tone\ToneInterface;
use Drupal\tone\ToneTypeInterface;

/**
 * @coversDefaultClass \Drupal\tone\ToneExtraFields
 *
 * @group tone
 */
class ToneExtraFieldsTest extends KernelTestBase {

  use EntityReferenceFieldCreationTrait;
  use ToneTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_test',
    'field',
    'tone',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('tone');
    $this->installEntitySchema('entity_test');
    $this->installEntitySchema('entity_test_rev');
  }

  /**
   * @covers ::getExtraFieldInfo
   * @covers ::getFieldMap
   * @covers ::getExtraFieldDefinition
   * @covers ::getReverseReferenceGraph
   * @covers ::getAllBundlesForEntityType
   */
  public function testGetExtraFieldsInfoEmpty(): void {
    $entityFieldManager = $this->container->get(EntityFieldManagerInterface::class);
    assert($entityFieldManager instanceof EntityFieldManagerInterface);
    $entityTypeBundleInfo = $this->container->get(EntityTypeBundleInfoInterface::class);
    assert($entityTypeBundleInfo instanceof EntityTypeBundleInfoInterface);
    $entityDisplayRepository = $this->container->get(EntityDisplayRepositoryInterface::class);
    assert($entityDisplayRepository instanceof EntityDisplayRepositoryInterface);
    $extraFields = new ToneExtraFields($entityFieldManager, $entityTypeBundleInfo, $entityDisplayRepository, 2);

    $extraFieldInfo = $extraFields->getExtraFieldInfo();
    $this->assertEmpty($extraFieldInfo);
  }

  /**
   * @covers ::getExtraFieldInfo
   * @covers ::getFieldMap
   * @covers ::getExtraFieldDefinition
   * @covers ::getReverseReferenceGraph
   * @covers ::getAllBundlesForEntityType
   */
  public function testGetExtraFieldsInfoDirectReference(): void {
    $entityFieldManager = $this->container->get(EntityFieldManagerInterface::class);
    assert($entityFieldManager instanceof EntityFieldManagerInterface);
    $entityTypeBundleInfo = $this->container->get(EntityTypeBundleInfoInterface::class);
    assert($entityTypeBundleInfo instanceof EntityTypeBundleInfoInterface);
    $entityDisplayRepository = $this->container->get(EntityDisplayRepositoryInterface::class);
    assert($entityDisplayRepository instanceof EntityDisplayRepositoryInterface);
    $extraFields = new ToneExtraFields($entityFieldManager, $entityTypeBundleInfo, $entityDisplayRepository, 2);

    $fieldLabel = $this->randomMachineName();
    $fieldName = 'field_' . strtolower($this->randomMachineName());

    $this->createToneType();
    $this->createEntityReferenceField('entity_test', 'entity_test', $fieldName, $fieldLabel, 'tone');

    $extraFieldInfo = $extraFields->getExtraFieldInfo();
    $toneExtraField = $extraFieldInfo['entity_test']['entity_test']['display']['tone__' . $fieldName];
    $this->assertIsArray($toneExtraField);
    $this->assertInstanceOf(TranslatableMarkup::class, $toneExtraField['label']);
    $this->assertEquals('Tone from ' . $fieldLabel, (string) $toneExtraField['label']);
    $this->assertInstanceOf(TranslatableMarkup::class, $toneExtraField['description']);
    $this->assertEquals(0, $toneExtraField['weight']);
    $this->assertFalse($toneExtraField['visible']);
  }

  /**
   * @covers ::getExtraFieldInfo
   * @covers ::getFieldMap
   * @covers ::getExtraFieldDefinition
   * @covers ::getReverseReferenceGraph
   * @covers ::getAllBundlesForEntityType
   */
  public function testGetExtraFieldsInfoRecursiveReference(): void {
    $entityFieldManager = $this->container->get(EntityFieldManagerInterface::class);
    assert($entityFieldManager instanceof EntityFieldManagerInterface);
    $entityTypeBundleInfo = $this->container->get(EntityTypeBundleInfoInterface::class);
    assert($entityTypeBundleInfo instanceof EntityTypeBundleInfoInterface);
    $entityDisplayRepository = $this->container->get(EntityDisplayRepositoryInterface::class);
    assert($entityDisplayRepository instanceof EntityDisplayRepositoryInterface);
    $extraFields = new ToneExtraFields($entityFieldManager, $entityTypeBundleInfo, $entityDisplayRepository, 2);

    $fieldLabel = $this->randomMachineName();
    $fieldName = 'field_' . strtolower($this->randomMachineName());
    $revLabel = $this->randomMachineName();
    $revName = 'field_' . strtolower($this->randomMachineName());

    $this->createToneType();
    $this->createEntityReferenceField('entity_test_rev', 'entity_test_rev', $fieldName, $fieldLabel, 'tone');
    $this->createEntityReferenceField('entity_test', 'entity_test', $revName, $revLabel, 'entity_test_rev');

    $extraFieldInfo = $extraFields->getExtraFieldInfo();
    $toneExtraField = $extraFieldInfo['entity_test']['entity_test']['display']['tone__' . $revName];
    $this->assertIsArray($toneExtraField);
    $this->assertInstanceOf(TranslatableMarkup::class, $toneExtraField['label']);
    $this->assertEquals('Tones from ' . $revLabel . ' (recursive)', (string) $toneExtraField['label']);
    $this->assertInstanceOf(TranslatableMarkup::class, $toneExtraField['description']);
    $this->assertEquals(0, $toneExtraField['weight']);
    $this->assertFalse($toneExtraField['visible']);
  }

  /**
   * @covers ::attach
   * @covers ::getFieldMap
   */
  public function testAttachDirectReference(): void {
    $selector = $this->randomMachineName();
    $propertyName = $this->randomMachineName();
    $fieldName = 'label';

    $bundle = $this->createToneType(
      selector: $selector,
      renderer: 'tone_css_property',
      rendererConfiguration: [
        'property_name' => $propertyName,
        'field_name' => $fieldName,
      ],
    );

    $fieldLabel = $this->randomMachineName();
    $fieldName = 'field_' . strtolower($this->randomMachineName());

    $this->createEntityReferenceField('entity_test', 'entity_test', $fieldName, $fieldLabel, 'tone');

    $displayRepository = $this->container->get(EntityDisplayRepositoryInterface::class);
    assert($displayRepository instanceof EntityDisplayRepositoryInterface);

    $displayRepository->getViewDisplay('entity_test', 'entity_test')
      // Display entity reference field in default view mode.
      ->setComponent($fieldName, [
        'type' => 'entity_reference_entity_view',
        'label' => 'hidden',
        'settings' => ['view_mode' => 'full'],
      ])
      // Display tone extra field in default view mode.
      ->setComponent('tone__' . $fieldName, ['region' => 'content'])
      ->save();

    $fieldValue = $this->randomMachineName();
    $tone = Tone::create([
      'label' => $fieldValue,
      'bundle' => $bundle->id(),
    ]);
    $tone->save();

    $parentEntity = EntityTest::create([$fieldName => $tone]);
    $parentEntity->save();

    $entityTypeManager = $this->container->get(EntityTypeManagerInterface::class);
    assert($entityTypeManager instanceof EntityTypeManagerInterface);
    $viewBuilder = $entityTypeManager->getViewBuilder('entity_test');
    $build = $viewBuilder->view($parentEntity);

    $renderer = $this->container->get(RendererInterface::class);
    assert($renderer instanceof RendererInterface);
    $renderer->renderInIsolation($build);

    $this->assertCssPublicFilesAttachment($tone, $build);
  }

  /**
   * @covers ::attach
   * @covers ::getFieldMap
   */
  public function testAttachRecursiveReference(): void {
    $selector = $this->randomMachineName();
    $propertyName = $this->randomMachineName();
    $fieldName = 'label';

    $bundle = $this->createToneType(
      selector: $selector,
      renderer: 'tone_css_property',
      rendererConfiguration: [
        'property_name' => $propertyName,
        'field_name' => $fieldName,
      ],
    );

    $fieldLabel = $this->randomMachineName();
    $fieldName = 'field_' . strtolower($this->randomMachineName());
    $revLabel = $this->randomMachineName();
    $revName = 'field_' . strtolower($this->randomMachineName());

    $this->createEntityReferenceField('entity_test_rev', 'entity_test_rev', $fieldName, $fieldLabel, 'tone');
    $this->createEntityReferenceField('entity_test', 'entity_test', $revName, $revLabel, 'entity_test_rev');

    $displayRepository = $this->container->get(EntityDisplayRepositoryInterface::class);
    assert($displayRepository instanceof EntityDisplayRepositoryInterface);

    $displayRepository->getViewDisplay('entity_test_rev', 'entity_test_rev')
      // Display entity reference field in default view mode.
      ->setComponent($fieldName, [
        'type' => 'entity_reference_entity_view',
        'label' => 'hidden',
        'settings' => ['view_mode' => 'full'],
      ])
      // Display tone extra field in default view mode.
      ->setComponent('tone__' . $fieldName, ['region' => 'content'])
      ->save();

    $displayRepository->getViewDisplay('entity_test', 'entity_test')
      // Display entity reference field in default view mode.
      ->setComponent($revName, [
        'type' => 'entity_reference_entity_view',
        'label' => 'hidden',
        'settings' => ['view_mode' => 'default'],
      ])
      // Display tone extra field in default view mode.
      ->setComponent('tone__' . $revName, ['region' => 'content'])
      ->save();

    $fieldValue = $this->randomMachineName();
    $tone = Tone::create([
      'label' => $fieldValue,
      'bundle' => $bundle->id(),
    ]);
    $tone->save();

    $parentEntity = EntityTestRev::create([$fieldName => $tone]);
    $parentEntity->save();

    $topEntity = EntityTest::create([$revName => $parentEntity]);
    $topEntity->save();

    $entityTypeManager = $this->container->get(EntityTypeManagerInterface::class);
    assert($entityTypeManager instanceof EntityTypeManagerInterface);
    $viewBuilder = $entityTypeManager->getViewBuilder('entity_test');
    $build = $viewBuilder->view($parentEntity);

    $renderer = $this->container->get(RendererInterface::class);
    assert($renderer instanceof RendererInterface);
    $renderer->renderInIsolation($build);

    $this->assertCssPublicFilesAttachment($tone, $build);
  }

  /**
   * Asserts that a HTML head link to a tone css file is attached.
   *
   * @param \Drupal\tone\ToneInterface $tone
   *   The tone entity.
   * @param mixed[] $build
   *   The render array.
   */
  protected function assertCssPublicFilesAttachment(ToneInterface $tone, array $build): void {
    $toneType = $tone->get('bundle')->entity;
    assert($toneType instanceof ToneTypeInterface);
    $identity = $toneType->getIdentity();
    $identifier = $identity->getIdentifier($tone, $build['#view_mode']);

    $fileUrlGenerator = $this->container->get('file_url_generator');
    assert($fileUrlGenerator instanceof FileUrlGeneratorInterface);
    $expectedUrlPrefix = $fileUrlGenerator->generateString("public://tone_css/tone-{$identifier}.css");
    assert(!empty($expectedUrlPrefix));

    $url = $build['#attached']['html_head_link'][0][0]['href'];
    $this->assertStringStartsWith($expectedUrlPrefix, $url);
  }

}

<?php

namespace Drupal\Tests\tone\Kernel\Identity;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\tone\Traits\ToneTestTrait;
use Drupal\tone\Entity\Tone;
use Drupal\tone\Plugin\ToneIdentity\Hash;

/**
 * @coversDefaultClass \Drupal\tone\Plugin\ToneIdentity\Hash
 *
 * @group tone
 */
class HashTest extends KernelTestBase {

  use ToneTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['tone', 'user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('tone');
  }

  /**
   * Tests hash identity.
   *
   * @covers ::getIdentifier
   * @covers ::getAttributes
   * @covers ::getSelector
   */
  public function testHashIdentity(): void {
    $prefix = $this->randomMachineName();
    $suffix = $this->randomMachineName();
    $selector = "{$prefix} & {$suffix}";

    $identityConfig = [
      'algorithm' => 'fnv1a64',
    ];

    $bundle = $this->createToneType(
      selector: $selector,
      identity: 'tone_hash',
      identityConfiguration: $identityConfig
    );

    $tone = Tone::create([
      'label' => $this->randomString(),
      'bundle' => $bundle->id(),
    ]);
    $tone->save();

    $identity = $bundle->getIdentity();
    $this->assertInstanceOf(Hash::class, $identity);

    $defaultIdentifier = $identity->getIdentifier($tone, 'default');
    $customIdentifier = $identity->getIdentifier($tone, 'custom_mode');
    $this->assertNotEquals($customIdentifier, $defaultIdentifier);

    $defaultAttributes = $identity->getAttributes($tone, 'default');
    $this->assertEquals(['data-tone-hash' => $defaultIdentifier], $defaultAttributes);
    $customAttributes = $identity->getAttributes($tone, 'custom_mode');
    $this->assertEquals(['data-tone-hash' => $customIdentifier], $customAttributes);

    $defaultSelector = $identity->getSelector($tone, 'default');
    $this->assertEquals("{$prefix} [data-tone-hash=\"{$defaultIdentifier}\"] {$suffix}", $defaultSelector);
    $customSelector = $identity->getSelector($tone, 'custom_mode');
    $this->assertEquals("{$prefix} [data-tone-hash=\"{$customIdentifier}\"] {$suffix}", $customSelector);
  }

}

<?php

namespace Drupal\Tests\tone\Kernel\Identity;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\tone\Traits\ToneTestTrait;
use Drupal\tone\Entity\Tone;
use Drupal\tone\Plugin\ToneIdentity\EntityUuid;

/**
 * @coversDefaultClass \Drupal\tone\Plugin\ToneIdentity\EntityUuid
 *
 * @group tone
 */
class EntityUuidTest extends KernelTestBase {

  use ToneTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['tone', 'user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('tone');
  }

  /**
   * Tests hash identity.
   *
   * @covers ::getIdentifier
   * @covers ::getAttributes
   * @covers ::getSelector
   * @covers ::getName
   */
  public function testEntityUuidIdentity(): void {
    $prefix = $this->randomMachineName();
    $suffix = $this->randomMachineName();
    $selector = "{$prefix} & {$suffix}";

    $bundle = $this->createToneType(
      id: 'sOmE_tone-TYPE',
      selector: $selector,
      identity: 'tone_entity_uuid'
    );

    $tone = Tone::create([
      'label' => $this->randomString(),
      'bundle' => $bundle->id(),
    ]);
    $tone->save();

    $identity = $bundle->getIdentity();
    $this->assertInstanceOf(EntityUuid::class, $identity);

    $defaultIdentifier = $identity->getIdentifier($tone, 'default');
    $this->assertEquals("{$tone->uuid->value}--default", $defaultIdentifier);
    $customIdentifier = $identity->getIdentifier($tone, 'custom_mode');
    $this->assertEquals("{$tone->uuid->value}--custom-mode", $customIdentifier);
    $this->assertNotEquals($customIdentifier, $defaultIdentifier);

    $defaultAttributes = $identity->getAttributes($tone, 'default');
    $this->assertEquals(["data-tone--some-tone-type" => $defaultIdentifier], $defaultAttributes);
    $customAttributes = $identity->getAttributes($tone, 'custom_mode');
    $this->assertEquals(['data-tone--some-tone-type' => $customIdentifier], $customAttributes);

    $defaultSelector = $identity->getSelector($tone, 'default');
    $this->assertEquals("{$prefix} [data-tone--some-tone-type=\"{$defaultIdentifier}\"] {$suffix}", $defaultSelector);
    $customSelector = $identity->getSelector($tone, 'custom_mode');
    $this->assertEquals("{$prefix} [data-tone--some-tone-type=\"{$customIdentifier}\"] {$suffix}", $customSelector);
  }

}

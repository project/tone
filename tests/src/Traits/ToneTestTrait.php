<?php

namespace Drupal\Tests\tone\Traits;

use Drupal\tone\Entity\ToneType;
use Drupal\tone\ToneTypeInterface;

/**
 * Provides common functionality for the Tone test classes.
 */
trait ToneTestTrait {

  /**
   * Creates a tone type.
   *
   * @param string|null $id
   *   The tone type ID.
   * @param string|null $label
   *   The tone type label.
   * @param string|null $selector
   *   The tone type selector.
   * @param string|null $identity
   *   The ID of the identity plugin to use.
   * @param mixed[] $identityConfiguration
   *   The identity plugin configuration.
   * @param string|null $renderer
   *   The ID of the renderer plugin to use.
   * @param mixed[] $rendererConfiguration
   *   The renderer plugin configuration.
   * @param string|null $attachmentStrategy
   *   The ID of the attachment strategy to use.
   * @param mixed[] $attachmentStrategyConfiguration
   *   The attachment strategy plugin configuration.
   *
   * @return \Drupal\tone\ToneTypeInterface
   *   The new tone type.
   */
  public function createToneType(
    ?string $id = NULL,
    ?string $label = NULL,
    ?string $selector = NULL,
    ?string $identity = NULL,
    array $identityConfiguration = [],
    ?string $renderer = NULL,
    array $rendererConfiguration = [],
    ?string $attachmentStrategy = NULL,
    array $attachmentStrategyConfiguration = [],
  ): ToneTypeInterface {
    $bundle = ToneType::create([
      'id' => $id ?? $this->randomMachineName(),
      'label' => $label ?? $this->randomString(),
      'selector' => $selector ?? '&',
      'identity' => $identity ?? 'tone_entity_uuid',
      'identity_configuration' => $identityConfiguration,
      'renderer' => $renderer ?? 'tone_css_property',
      'renderer_configuration' => $rendererConfiguration,
      'attachment_strategy' => $attachmentStrategy ?? 'tone_css_public_files',
      'attachment_strategy_configuration' => $attachmentStrategyConfiguration,
    ]);
    $bundle->save();
    return $bundle;
  }

}
